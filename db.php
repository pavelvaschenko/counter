<?php
require 'cookies.php';
class DB
{

    private $db_server;
    private $db_username;
    private $db_password;
    private $db_name;
    private $db_users_table;
    private $db_link;
    private $current_page;
    private $ip_addr;
    private $inc_interval;
    private $inc_total;

    function __construct()
    {
        $this->db_server = 'localhost';
        $this->db_username = 'root';
        $this->db_password = 'root';
        $this->db_name = 'counter';
        $this->db_users_table = 'users';
        $this->current_page = realpath(basename($_SERVER['PHP_SELF']));
        $this->current_page = str_replace('\\', '/', $this->current_page);
        $this->ip_addr = $_SERVER['REMOTE_ADDR'];
        $this->inc_interval = 86400;
        $this->inc_total = TRUE;
    }

    function updateCounter($autoConnect = TRUE)
    {
        if ($autoConnect)
        {
            $this->db_link = mysqli_connect($this->db_server, $this->db_username,
                                           $this->db_password) or die(mysqli_error($this->db_link));
            mysqli_select_db($this->db_link, $this->db_name) or die(mysqli_error($this->db_link));
        }
        $this->installTables();
        $this->updateUsers();
    }

    function getActiveVisits($interval = 30)
    {
        $countFrom = time() - $interval;
        $result = mysqli_query($this->db_link, "SELECT COUNT(*) FROM $this->db_users_table
            WHERE sc_time >= $countFrom") or die(mysqli_error($this->db_link));
        return mysqli_fetch_array($result)[0];
    }

    function installTables()
    {
        $foundUsers = FALSE;
        $result = mysqli_query($this->db_link, 'SHOW TABLES') or die(mysql_error());
        while ($row = mysqli_fetch_array($result))
        {
            if (strtoupper($this->db_users_table) == strtoupper($row[0]))
            {
                $foundUsers = TRUE;
            }
            if ($foundUsers)
            {
                break;
            }
        }
        if (!$foundUsers)
        {
            mysqli_query($this->db_link, "CREATE TABLE $this->db_users_table(
                            sc_ip CHAR(16) NOT NULL,
                            sc_time INT UNSIGNED NOT NULL,
                            sc_location VARCHAR(255) NOT NULL DEFAULT '',
                            PRIMARY KEY(sc_ip)
            )") or die(mysqli_error($this->db_link));
        }
    }

    function updateUsers()
    {
        $now = time();
        $cookie_key = 'online-cache';
        $dayStart = $now - 86400;
        mysqli_query($this->db_link, "DELETE FROM $this->db_users_table WHERE sc_time < $dayStart",
                    ) or die(mysqli_error($this->db_link));
        $currentPage = $this->current_page;
        $currentPage = addslashes($currentPage);
        $result = mysqli_query($this->db_link, "SELECT sc_time FROM $this->db_users_table
      WHERE sc_ip = '$this->ip_addr'") or die(mysqli_error($this->db_link));
        if (!mysqli_num_rows($result))
        {
            mysqli_query($this->db_link, "INSERT INTO $this->db_users_table (sc_ip, sc_time, sc_location)
        VALUES ('$this->ip_addr', $now, '$currentPage')")
            or die(mysqli_error($this->db_link));
            $this->inc_total = TRUE;
        }
        else
        {
            $do_update = false;
            if ( CookieManager::stored($cookie_key) )
            {
                $c = (array) @json_decode(CookieManager::read($cookie_key), true);
                if ( $c )
                {
                    //обновляем данные в базе каждые 5 минут
                    if( $c['lastvisit'] < (time() - (60 * 5)) )
                    {
                        $do_update = true;
                    }
                } else
                {
                    $do_update = true;
                }

            } else{
                $do_update = true;
            }
            if ( $do_update )
            {
                $lastTime = mysqli_fetch_array($result)[0];
                $this->inc_total = (($now - $lastTime) > $this->inc_interval);
                mysqli_query($this->db_link, "UPDATE $this->db_users_table SET sc_time = $now,
                sc_location = '$currentPage' WHERE sc_ip = '$this->ip_addr'") or die(mysqli_error($this->db_link));
                CookieManager::store($cookie_key, json_encode(['id' => mysqli_insert_id($this->db_link), 'lastvisit' => $now]));
            }
        }
    }
}
